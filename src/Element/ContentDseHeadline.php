<?php

/**
 * 361GRAD Element Headline
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementHeadline\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\StringUtil;
use Patchwork\Utf8;

/**
 * Class ContentDseHeadline
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDseHeadline extends ContentElement
{
    /**
     * Template name.
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_headline';


    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        return parent::generate();
    }


    /**
     * Generate the module
     *
     * @return void
     */
    protected function compile()
    {
        $arrSubheadline              = StringUtil::deserialize($this->dse_subheadline);
        $this->Template->subheadline = is_array($arrSubheadline) ? $arrSubheadline['value'] : $arrSubheadline;
        $this->Template->shl         = is_array($arrSubheadline) ? $arrSubheadline['unit'] : 'h2';
    }
}
